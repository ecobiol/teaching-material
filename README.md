<p align="center" width="100%">
<img width="25%" src="img/PigletteGoesToSchool.svg"/>
</p>

# Teaching Materials for Systems Biology

We created this project to collect teaching material for researchers and students interested in Systems Biology. In our GitLab repository, you can find lectures, transcripts, slides, tutorials and other material that might help your teaching. You can find all the collected files in the different subfolders. An overview of materials can be found below.

## How you can contribute

We invite everyone to contribute their material to this repository. All you need is to follow a few simple rules:
- We require that the files you upload are shared under a [Creative Commons](https://creativecommons.org/) license. Please specify one of the four open licenses ("CC BY" - our recommendation, "CC BY SA", "CC BY NC", or "CC BY NC SA"). Please read what that means if you are not familiar with it. 
- Then, make sure that you own the copyright, or that you have permission to share the art from its owners. We have all been there, when using an image or graphic that we found through an online search. These are usually protected by copyright, and must be removed from your slides before uploading them.
- Ideally, create a Pull Request with the new files on this repository. Don't forget to add an extra file with your name, affiliation, and the date (and any other information you feel is relevant) so we can keep track of who contributed what.
- If you don't feel comfortable with Git, you can also just email your files to us directly, and we'll take care of it.

That's it! We hope that this repository will grow quickly and be used by as many people as possible. Of course, any suggestions on how to improve it are very welcome - you can use the [Issues](https://gitlab.com/ecobiol/teaching-material/-/issues) tab for that, or our dedicated [Gitter](https://gitter.im/ecobiol/community) channel. Also, if you'd like to join us as a maintainer of the project, let us know. We will be delighted.

## Collection of teaching materials

### Cell biology

We recommend the book "Cell biology by the numbers" by Ron Milo and Rob Phillips, related to the [BioNumbers](https://bionumbers.hms.harvard.edu/) database.
A draft version can be freely downloaded from [book.bionumbers.org](http://book.bionumbers.org/).
We also recommend watching the accompanying lectures by Ron Milo: 
* TEDX talk ["A sixth sense for understanding our cells"](https://www.youtube.com/watch?v=JC7WnzM2Lsc) 
* Full BioNumbers lectures on [youtube](https://www.youtube.com/channel/UChCzuzoZp5NheAPWH5YoGWw)

### Systems biology

* [Lecture slides "Introduction to systems biology"](./systems-biology/systems-biology/SysBio_2020_Lecture_1_Introduction_to_systems_biology.pdf) (W. Liebermeister, CRI 2020) 
* [Lecture slides "High-throughput experiments - The omics revolution in Systems Biology"](./systems-biology/systems-biology/SysBio_2022_Lecture_2_Jules_High-throughput-experiments.pdf) (M. Jules, CRI 2022) 
* [Lecture slides "From data to models in molecular biology (part 1)"](./systems-biology/systems-biology/SysBio_2022_Lecture_5_Calzone_Martignetti_From_data_to_models_1.pdf) (L. Calzone and L. Martignetti, CRI 2022) 
* [Lecture slides "From data to models in molecular biology (part 2)"](./systems-biology/systems-biology/SysBio_2022_Lecture_5_Calzone_Martignetti_From_data_to_models_2.pdf) (L. Calzone and L. Martignetti, CRI 2022) 

### Cellular networks

* [Course script on cellular networks](./systems-biology/cellular-networks/Cellular_Networks_WS2005.pdf) (HU Berlin Student course, 2005, 2005)
* [Lecture slides "Biochemical network models"](./systems-biology/cellular-networks/SysBio_2020_Lecture_2_Biochemical_network_models.pdf)  (W. Liebermeister, CRI 2020)
* [Blackboard session on qualitative dynamical modeling](./systems-biology/cellular-networks/logical-modeling) (D. Thieffry, blackboard session at CompSysBio2021 workshop) 
* [Lecture slides "Metabolic networks"](./systems-biology/cellular-networks/SysBio_2020_Lecture_3a_Metabolic_networks.pdf) (W. Liebermeister, CRI 2020)
* [Lecture script "Network motifs"](./systems-biology/cellular-networks/Lecture_Modelling_of_cell_processes_2009/course_script/script_4_network_motifs.pdf) (W. Liebermeister, 2009) 
* [Lecture slides "Network motifs"](./systems-biology/cellular-networks/SysBio_2020_Lecture_3b_Network_motifs.pdf) (W. Liebermeister, CRI 2020)

### Regulation of gene expression

* [Lecture slides "Gene expression regulation and noise"](./systems-biology/gene-expression-regulation/SysBio_2022_Lecture_3_Jules_Gene-expression-regulation-and-noise.pdf) (M. Jules, CRI 2022)

### Metabolic models

* [Course script on enzyme kinetics and MCA (with exercises)](./systems-biology/metabolic-models/Enzyme_Kinetics_MCA_WS2009.pdf) (HU Berlin Student course, 2009, 2009)
* [Lecture script "Kinetic models"](./systems-biology/cellular-networks/Lecture_Modelling_of_cell_processes_2009/course_script/script_2_kinetic_models.pdf) (W. Liebermeister, 2009) 
* [Blackboard session SysBio 2016 1](./systems-biology/metabolic-models/Innsbruck_course_2016) (W. Liebermeister, SysBio course Innsbruck 2016)
* [Blackboard session SysBio 2018 1](./systems-biology/metabolic-models/Innsbruck_course_2018)/[2](./systems-biology/metabolic-models/metabolic-modeling-Innsbruck_Course_2018),  (E. Noor, W. Liebermeister, SysBio course Innsbruck 2018)
* [Blackboard session on integrated networks of metabolism and gene expression](./systems-biology/metabolic-models/network-of-metabolism-and-gene-expression-CompSysBio2021) (H. de Jong, 2021, CompSysBio2021 workshop) 
* [Modeling of metabolic and gene regulatory networks](https://team.inria.fr/microcosme/course-on-modeling-of-metabolic-and-gene-regulatory-networks-insa-de-lyon-2022-2023/) (H. de Jong, 2022) 

### Thermodynamics and bioenergetics

* [Lecture "Cell bioenergetics"](./systems-biology/thermodynamics-and-bioenergetics/20230202-Lecture_Script_QBio301-2.pdf) (O. Ebenhoeh)
* [Presentation "Introduction to thermodynamics"](./systems-biology/thermodynamics-and-bioenergetics/thermodynamicsIntroduction.odp) (O. Ebenhoeh)
* [Blackboard session "The laws of thermodynamics: applications in models of metabolism"](./systems-biology/thermodynamics-and-bioenergetics/laws-of-thermodynamics-CompSysBio2021) (E. Noor) 
* [Talk "A simple thermodynamic relation"](./systems-biology/thermodynamics-and-bioenergetics/Lorentz_Center_2018) (W. Liebermeister) 

### Resource allocation and enzyme economics

* [Lecture "Whole-cell models"](./systems-biology/resource-allocation/PSL-ITI_Whole_Cell_Modeling_2017) (W. Liebermeister, 2017) 
* [Online course "Resource allocation models"](./systems-biology/resource-allocation/Lab_course_Milo_group_2020) (W. Liebermeister) 
* [Lecture slides "Resource allocation and growth"](./systems-biology/resource-allocation/SysBio_2021_Lecture_5_Bertaux_Resource_Allocation.pdf) (F. Bertaux, CRI 2021)
* [Blackboard session "Enzyme economy in metabolic models" 2021](./systems-biology/resource-allocation/CompSysBio2021) (W. Liebermeister)

### Economy of the cell

Lectures from the [EPCP summer school Paris 2022](https://principlescellphysiology.org/summer-school-2022/index.html)

Lectures from the [EPCP summer school Paris 2023](https://principlescellphysiology.org/summer-school-2023/index.html)

Lectures from the [EPCP summer school Paris 2024](https://principlescellphysiology.org/summer-school-2024/index.html)

Exercises

* [Cell components](./systems-biology/economy-of-the-cell/epcp-summer-school-2022/EPCP_Paris_2022_Exercise_CEN_What_makes_up_a_cell.pdf) (D. Szeliova)
* [Balanced growth](./systems-biology/economy-of-the-cell/epcp-summer-school-2022/EPCP_Paris_2022_Exercise_BAL_Balanced_cell_growth.pdf) (M. Wortel)
* [FBA (jupyter notebook)](./systems-biology/economy-of-the-cell/epcp-summer-school-2022/EPCP_Paris_2022_Exercise_FLX_Flux_balance_analysis.ipynb) (S. Waldherr)
* [Pathway fluxes and cost](./systems-biology/economy-of-the-cell/epcp-summer-school-2022/EPCP_Paris_2022_Exercise_PAT_Cost_of_metabolic_pathways.pdf) (E. Noor)
* [Optimal metabolic states](./systems-biology/economy-of-the-cell/epcp-summer-school-2022/EPCP_Paris_2022_Exercise_OME_Optimal_metabolic_states.pdf) (M. Wortel)

### Cell interactions, population dynamics, and evolution

* [Lecture slides "Compromise, competition and cooperation"](./systems-biology/population-dynamics/SysBio_2020_Lecture_8_Compromise_competition_and_cooperation.pdf) (W. Liebermeister, CRI 2020)
* [Lecture slides "Population dynamics and evolution"](./systems-biology/population-dynamics/SysBio_2021_Lecture_6_Bertaux_Population_Dynamics_Evolution.pdf) (F. Bertaux, CRI 2021)
* Presentation slides and videos on various topics from the "Advanced School on Quantitative Principles in Microbial Physiology: from Single Cells to Cell Communities" can be found at https://indico.ictp.it/event/10213/other-view?view=ictptimetable

### Mathematical modeling and bioinformatics

* [Course script on dynamical systems](./systems-biology/modeling-and-bioinformatics/Dynamical_Systems_WS2009.pdf) (HU Berlin Student course, 2009)
* [Lecture script "Modeling formalisms besides kinetic models" (in German)](./systems-biology/cellular-networks/Lecture_Modelling_of_cell_processes_2009/course_script/script_3_other_formalisms_german.pdf) (W. Liebermeister, 2009) 
* [Blackboard session on multistability, bifurcation, and differentiation](./systems-biology/modeling-and-bioinformatics/multistability) (D. Gonze, blackboard session at CompSysBio 2021 workshop) 
* [Course script on parameter estimation and model selection](./systems-biology/modeling-and-bioinformatics/Parameter_Estimation_Model_discrimination_WS2009.pdf) (HU Berlin Student course, 2009)
* [Lecture script "Model reduction and coupling"](./systems-biology/cellular-networks/Lecture_Modelling_of_cell_processes_2009/course_script/script_6_model_reduction_and_coupling.pdf) (W. Liebermeister, 2009) 
* [Course script on stochastic processes](./systems-biology/modeling-and-bioinformatics/Stochastic_processes_SS2009.pdf) (HU Berlin Student course, 2009)
* [Course script on sequence analysis](./systems-biology/modeling-and-bioinformatics/Sequence_Analysis_SS2005.pdf) (HU Berlin Student course, 2005)

### Models of cellular subsystems

* [Course script on signaling pathways](./systems-biology/cellular-subsystems/Signaling_Pathways_WS2009.pdf) (HU Berlin Student course, 2009)
* [Course script on gene expression models](./systems-biology/cellular-subsystems/Gene_Expression_WS2004.pdf) (HU Berlin Student course, 2004)
* [Lecture script "Gene input functions"](./systems-biology/cellular-networks/Lecture_Modelling_of_cell_processes_2009/course_script/script_5_gene_input_functions.pdf) (W. Liebermeister, 2009) 
* [Course script on calcium dynamics](./systems-biology/cellular-subsystems/Calcium_Dynamics_WS2004.pdf) (HU Berlin Student course, 2004)
* [Course script on cell cycle models](./systems-biology/cellular-subsystems/Cell_Cycle_WS2009.pdf) (HU Berlin Student course, 2009)
* [Course script on circadian rhythms](./systems-biology/cellular-subsystems/Circadian_Rhythms_WS2007.pdf) (HU Berlin Student course, 2007)
* [Course script on neuronal dynamics](./systems-biology/cellular-subsystems/Neuronal_Dynamics_WS2007.pdf) (HU Berlin Student course, 2007)
* [Course script on DNA repair](./systems-biology/cellular-subsystems/DNA_Repair_SS2006.pdf) (HU Berlin Student course, 2006)
* [Course script on chemotaxis](./systems-biology/cellular-subsystems/Chemotaxis_WS2007.pdf) (HU Berlin Student course, 2007)

### Dynamics of biological processes

* [Course script on pattern formation](./systems-biology/biological-dynamics/Pattern_Formation_SS2007.pdf) (HU Berlin Student course, 2007)
* [Course script on population kinetics](./systems-biology/biological-dynamics/Population_Kinetics_SS2006.pdf) (HU Berlin Student course, 2006)
* [Course script on selection equations](./systems-biology/biological-dynamics/Selection_Equations_SS2009.pdf) (HU Berlin Student course, 2009)

## Sources

### Systems biology and modeling block courses at HU Berlin, Theoretical Biophysics

The scripts marked as "HU Berlin student course" originate from different courses for biophysics students (Theoretical Biophysics lab at Humboldt University Berlin) held between 2004 and 2009 (SS: summer semester; WS: winter semester). Courtesy of E. Klipp and many (then) members of the group who developed the course scripts. For other scripts and exercises (e.g., introductions to python, matlab, R, Copasi, SBML), please contact W. Liebermeister.

### Center for Interdisciplinary Research (CRI)

Files marked as "CRI" stem from systems biology classes held by different teachers at the Center for interdisciplinary research (now Learning Planet Institute) Paris
