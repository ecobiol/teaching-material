Blackboard teaching on "Multistability, bifurcation, and differentiation"
-----------------------------------------------------------

* held by: D. Gonze
* held at: [CompSysBio - Advanced Lecture Course on Computational Systems Biology](https://project.inria.fr/compsysbio2021/), Aussois 2021
* held in: November 2021

Contact: D. Gonze

-----------------------------------------------------------
### Multistability, bifurcation, and differentiation

Didier Gonze

Université Libre de Bruxelles

During embryonic development, cells divide and successively differentiate to acquire specific identities. This differentiation process is controlled by gene regulatory networks that typically display bi- or multistability. We will see how to build and to analyze such multistable systems based on ODE and bifurcation theory. We will then describe a particular model developed to account for cell fate specification during early embryonic development in mammals. These models will be simulated with Matlab/Octave and/or XPP-AUTO ( http://homepages.ulb.ac.be/~dgonze/AUSSOIS2021/  ;  http://www.math.pitt.edu/~bard/xpp/xpp.html).