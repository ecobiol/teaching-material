### Teaching materials "Metabolic models"

* [Systems biology courses ETH Zürich](jupyter-notebook-exercises-sysbio_ETH_course_2019) Jupyter notebooks covering different aspects of cell modeling (from ETH Systems Biology Course 2019) `EPCP_Paris_2022_Exercise_FLX_Flux_balance_analysis.ipynb`
* [Course script on enzyme kinetics and MCA (with exercises)](Enzyme_Kinetics_MCA_WS2009.pdf) (HU Berlin Student course, 2009, 2009)
* [Lecture script "Kinetic models"](./systems-biology/cellular-networks/Lecture_Modelling_of_cell_processes_2009/course_script/script_2_kinetic_models.pdf) (W. Liebermeister, 2009) 
* [Blackboard session SysBio 2016 1](Innsbruck_course_2016) (W. Liebermeister, SysBio course Innsbruck 2016)
* [Blackboard session SysBio 2018 1](Innsbruck_course_2018)/[2](metabolic-modeling-Innsbruck_Course_2018),  (E. Noor, W. Liebermeister, SysBio course Innsbruck 2018)
* [Blackboard session on integrated networks of metabolism and gene expression](network-of-metabolism-and-gene-expression-CompSysBio2021) (H. de Jong, 2021, CompSysBio2021 workshop) 
* [Modeling of metabolic and gene regulatory networks](https://team.inria.fr/microcosme/course-on-modeling-of-metabolic-and-gene-regulatory-networks-insa-de-lyon-2022-2023/) (H. de Jong, 2022) 
