Blackboard session in the SysBio 2018 conference in Innsbruck, held by Elad Noor
-------------------------------------------------------------------------

* held by: Elad Noor
* held at: Advanced lecture course on systems biology, Innsbruck 2018
* held in: Spring 2018

Topics:
- metabolic modelling
- flux balance analysis
- thermodynamic flux analysis
- metabolic engineering methods