from scipy.interpolate import UnivariateSpline
from bokeh import io, models, plotting
import numpy as np
io.output_notebook()

def volcano(log2FC: np.array, pvals: np.array, labels: np.array):
    """Function for creating a Volcano plot.
    
    :param log2FC: a 1-D NumPy array with the log2 fold changes
    :param pvals: a 1-D NumPy array with the p-values
    :param labels: a 1-D NumPy array with the labels
    """
    minus_log10_p = -np.log10(pvals)
    desc = [f"{l}, fold-change = {f:.2g}, p-value = {p:.2g}"
            for f, p, l in zip(2**log2FC, pvals, labels)]

    source = models.ColumnDataSource(data=dict(x=log2FC, y=minus_log10_p, desc=desc))
    hover = models.HoverTool(tooltips=[
        ('data', '@desc'),
    ])
    p = plotting.figure(plot_width=400, plot_height=400, tools=[hover], title="Volcano plot")
    p.circle('x', 'y', size=6, source=source)
    p.xaxis[0].axis_label = 'log2 (fold change)'
    p.yaxis[0].axis_label = '-log10 (p-value)'
    io.show(p)
    
def qvalue(pvals: np.array) -> np.array:
    """Function for estimating q-values from p-values using the Storey-
    Tibshirani q-value method (2003).
    
    :param pvals: a 1-D NumPy array with P-values corresponding to a 
    family of hypotheses.
    :return: a 1-D NumPy array of the same shape and order with the 
    Q-values.
    """

    # Count the p-values. Find indices for sorting the p-values into
    # ascending order and for reversing the order back to original.
    m = pvals.shape[0]
    ind = np.argsort(pvals)
    rev_ind = np.argsort(ind)
    pvals = pvals[ind]

    # Estimate proportion of features that are truly null.
    kappa = np.arange(0, 0.96, 0.01)
    pik = [sum(pvals > k) / (m*(1-k)) for k in kappa]
    cs = UnivariateSpline(kappa, pik, k=3, s=None, ext=0)
    pi0 = float(cs(1.))

    # The smoothing step can sometimes converge outside the interval [0, 1].
    # This was noted in the published literature at least by Reiss and
    # colleagues [4]. There are at least two approaches one could use to
    # attempt to fix the issue:
    # (1) Set the estimate to 1 if it is outside the interval, which is the
    #     assumption in the classic FDR method.
    # (2) Assume that if pi0 > 1, it was overestimated, and if pi0 < 0, it
    #     was underestimated. Set to 0 or 1 depending on which case occurs.
    # Here we have chosen the second option.
    if pi0 < 0:
        pi0 = 0
    elif pi0 > 1:
        pi0 = 1

    # Compute the q-values.
    qvals = np.zeros(np.shape(pvals))
    qvals[-1] = pi0*pvals[-1]
    for i in range(m-2, -1, -1):
        qvals[i] = min(pi0*m*pvals[i]/float(i+1), qvals[i+1])

    # Order the q-values according to the original order of the p-values.
    qvals = qvals[rev_ind]
    return qvals