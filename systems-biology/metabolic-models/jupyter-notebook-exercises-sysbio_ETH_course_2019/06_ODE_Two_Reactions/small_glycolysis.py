from pysb import *

Model()

# parameters
Parameter('CarbonInflux', 2)
Parameter('Km_gly', 20)
Parameter('Vmax_gly', 1.99)
Parameter('Km_pyr', 20)
Parameter('Vmax_pyr', 1.99)
Parameter('E_0', 1)
Parameter('S_0', 10)
Parameter('P_0', 0)

# species
Monomer('E')
Monomer('S')
Monomer('P')

# Observables
Observable('obsE', E())
Observable('obsS', S())
Observable('obsP', P())

# initial
Initial(E(), E_0)
Initial(S(), S_0)
Initial(P(), P_0)

# reaction rates
Expression('v', Kcat*obsE*obsS /(obsS+Km))

Rule('reaction', S() >> P(), v)