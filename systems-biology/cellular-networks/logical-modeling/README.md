Blackboard teaching: Qualitative dynamical modeling of cellular networks
------------------------------------------------------------------------

* held by: Denis Thieffry
* held at: [CompSysBio - Advanced Lecture Course on Computational Systems Biology](https://project.inria.fr/compsysbio2021/), Aussois 2021
* held in: November 2021

-----------------------
### Qualitative dynamical modeling of cellular networks

Denis Thieffry

Ecole Normale Supérieure, Paris

The first session will be devoted to a didactical introduction to Boolean dynamical modelling of regulatory/signaling networks. Through simple examples, we will emphasise key differences between the most used updating methods and discuss their limitations. These examples will further enable us to contrast the behaviours positive and negative circuits regulatory circuits, from both dynamical and biological point of views. We will also review several extensions of the Boolean formalism, which were specifically developped to model more precisely the behaviour of biological networks. Finally, we will stress some of the computational challenges arising with large/complex regulatory networks, and discuss some of the current approaches to cope with these challenges.

The second session will be devoted to a tutorial on the use of the software GINsim (http://ginsim.org). Using this software, participants will be trained to build and analyse a small but sophisticate multi-level logical model, therebyillustrating the concepts introduced during the course.

Students need to have GINsim installed on their laptop, with a JAVA version 1.7 or higher. Files with dependencies should be downloaded from:
http://ginsim.org/sites/default/files/GINsim-3.0.0b-with-deps.jar

Further reading:

A review to logical modelling:
https://www.frontiersin.org/articles/10.3389/fgene.2016.00094/full

An article specifically introducting the GINsim tutorial:
https://www.frontiersin.org/articles/10.3389/fphys.2018.00646/full
