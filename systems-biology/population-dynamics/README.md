### Teaching materials "Cell interactions, population dynamics, and evolution"

* [Lecture slides "Compromise, competition and cooperation"](SysBio_2020_Lecture_8_Compromise_competition_and_cooperation.pdf) (W. Liebermeister, CRI 2020)
* [Lecture slides "Population dynamics and evolution"](SysBio_2021_Lecture_6_Bertaux_Population_Dynamics_Evolution.pdf) (F. Bertaux, CRI 2021)

* Presentation slides and videos on various topics from the "Advanced School on Quantitative Principles in Microbial Physiology: from Single Cells to Cell Communities" can be found at https://indico.ictp.it/event/10213/other-view?view=ictptimetable
