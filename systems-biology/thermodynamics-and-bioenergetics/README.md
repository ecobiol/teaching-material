### Teaching materials "Thermodynamics and bioenergetics"

* [Lecture "Cell bioenergetics"](20230202-Lecture_Script_QBio301-2.pdf) (O. Ebenhoeh)
* [Presentation "Introduction to thermodynamics"](thermodynamicsIntroduction.odp) (O. Ebenhoeh)
* [Blackboard session "The laws of thermodynamics: applications in models of metabolism"](laws-of-thermodynamics-CompSysBio2021) (E. Noor) 
* [Talk "A simple thermodynamic relation"](Lorentz_Center_2018) (W. Liebermeister) 
