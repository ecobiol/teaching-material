# CompSysBio2021

Accompanying text for blackboard session in [Advanced Lecture Course on Computational System Biology 2021](https://project.inria.fr/compsysbio2021/).


## The laws of thermodynamics: applications in models of metabolism
Elad Noor
Weizmann Institute of Science

We will start with the basic laws of thermodynamics, and see a few fundamental implications in enzyme catalysis and metabolism (including flux direction constraints, the Haldane relationship, and the flux-force relationship). We will also learn how to apply these laws to constraint-based metabolic models, and when selecting parameters for kinetic models. Finally, we will see how thermodynamics can also be used in optimization problems, such as the Max-min Driving Force (MDF) algorithm. Exercises will be carried out in Python. A good MILP solver (CPLEX or GUROBI) is recommended but not necessary.

## Requirements
To take full advantage* of the session, it would be best to bring your own computer. Please clone this repository and make sure you have a [Jupyter Notebook](https://jupyter.org/install) installed in a [virtual environment](https://docs.python.org/3/tutorial/venv.html). It would be best to also install the following dependencies using `pip` or `conda` (you can also find the list in `notebooks/requirements.txt`):
- numpy~=1.21
- matplotlib~=3.4
- bokeh~=2.4
- scipy~=1.7
- cobra~=0.22
- escher~=1.7
- cvxpy~=1.1
- cvxopt~=1.2
- osqp~=0.6
- sympy~=1.9
- pandas~=1.3
- equilibrator-api==0.4.5
- equilibrator-pathway==0.4.4

If you are not sure what to do, just run the following commands in a shell environment (first, install python and git if you don't have them).
```bash
git clone https://gitlab.com/elad.noor/compsysbio2021.git
cd compsysbio2021/notebooks
python -m venv venv
source venv/bin/activate
```
The next two commands will download a lot of files and might take a while depending on your connection speed:
```bash
pip install -r requirements.txt
python -c "from equilibrator_api import ComponentContribution; cc = ComponentContribution()"
```
Finally, to start the Jupyter environment (that opens up inside your browser) type:
```bash
jupyter notebook
```

`*` If you don't manage to set this up, don't worry. We will have enough time to help you during the session.

## Bibliography
- Noor, E., Flamholz, A., Liebermeister, W., Bar-Even, A., and Milo, R. (2013). A note on the kinetics of enzyme action: A decomposition that highlights thermodynamic effects. FEBS Letters 587, 2772–2777.
- Noor, E., Bar-Even, A., Flamholz, A., Reznik, E., Liebermeister, W., and Milo, R. (2014). Pathway thermodynamics highlights kinetic obstacles in central metabolism. PLoS Computational Biology 10, e1003483.
- Noor, E. (2018). Removing both internal and unrealistic energy-generating cycles in Flux Balance Analysis. ArXiv:1803.04999 [q-Bio].



