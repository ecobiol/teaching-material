### Teaching materials "Dynamics of biological processes"

* [Course script on pattern formation](Pattern_Formation_SS2007.pdf) (HU Berlin Student course, 2007)
* [Course script on population kinetics](Population_Kinetics_SS2006.pdf) (HU Berlin Student course, 2006)
* [Course script on selection equations](Selection_Equations_SS2009.pdf) (HU Berlin Student course, 2009)
